package com.company;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {

        try (BufferedReader reader = new BufferedReader(new FileReader("test.txt"))) {
            ArrayList<String> list = new ArrayList<String>();
            int cnt;
            String temp = "";
            while ((cnt = reader.read()) != -1) {
                char symbol = (char) cnt;
                if ((!Character.isLetter(symbol)) && (temp != "")) {
                    list.add(temp);
                    temp = "";
                } else if (Character.isLetter(symbol)) {
                    temp += symbol;
                } else continue;
            }
            Collections.sort(list);
            int max = 0;
            ArrayList<Character> vowels = new ArrayList<Character>();
            vowels.add('A');
            vowels.add('a');
            vowels.add('E');
            vowels.add('e');
            vowels.add('Y');
            vowels.add('y');
            vowels.add('U');
            vowels.add('u');
            vowels.add('O');
            vowels.add('o');
            vowels.add('I');
            vowels.add('i');
            for (String word : list) {
                int count = 0;
                for(int i = 0; i < word.length(); i++)
                {
                    if (vowels.contains(word.charAt(i))) break;
                    count++;
                }
                if (count > max) max = count;
            }

            for (String word : list) {
                int count = 0;
                for(int i = 0; i < word.length(); i++)
                {
                    if (vowels.contains(word.charAt(i))) break;
                    count++;
                }
                if (count == max) System.out.println(word);
            }
        }
        catch (IOException e)
        {
            System.out.println("Exception");
        }

    }
}
